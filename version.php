<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * CU Grader Version file
 *
 * @package   local_cugrader
 */

/** @var object $plugin */
$plugin->version      = 2016081000;
$plugin->requires     = 2015111604; // Requires this Moodle version (3.0.4).
$plugin->component    = 'local_cugrader';
$plugin->release      = '3.0+ (Build: 20160810)';
$plugin->maturity     = MATURITY_STABLE;
$plugin->dependencies = array(
    'mod_hsuforum' => ANY_VERSION,
    'mod_assign'   => ANY_VERSION,
    'local_mr'     => ANY_VERSION,
);
